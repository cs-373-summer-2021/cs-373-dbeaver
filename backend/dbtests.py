from unittest import main, TestCase
from models import db, Book

class tests(TestCase):

    # --- Database Tests ---

    # Test: - Book - Insert and Remove
    def test_book_db(self):
        test = Book(title='example')
        db.session.add(test)
        db.session.commit()
        
        db.session.refresh(test)

        result = db.session.query(Book).filter_by(id = test.id).one()
        self.assertEqual(str(result.id), str(test.id))

        db.session.query(Book).filter_by(id = test.id).delete()
        db.session.commit()


if __name__ == "__main__":
    main()
