#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# Fares Fraij
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
# from marshmallow import fields
# from sqlalchemy.orm import column_property
# from sqlalchemy.sql.expression import and_, select, true
# from sqlalchemy.util.langhelpers import hybridproperty
import os

# initializing Flask app 
app = Flask(__name__) 

# Change this accordingly 
USER ="postgres"
PASSWORD ="postgres101"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="postgres"


# Make these command line arguments that provide when you deploy the app
# or use other options like connecting directly from App Engine


# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # To suppress a warning message
db = SQLAlchemy(app)
ma = Marshmallow(app)



link = db.Table('link',
   db.Column('author_id', db.Integer, db.ForeignKey('author.id')), 
   db.Column('book_id', db.Integer, db.ForeignKey('book.id'))
   ) 

# ------------
# Book
# ------------
class Book(db.Model):
    """
    Book class has two attrbiutes 
    title
    id
    """
    __tablename__ = 'book'
	
    title = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    # description = db.Column(db.String(250))
    price = db.Column(db.String(8))
    # This property refer to the "id" property in the "publisher" tabel
    publisher_id = db.Column(db.Integer, db.ForeignKey('publisher.id')) # (1-M)
    details = db.relationship('BookDetails', backref = 'book', uselist = False) # (1-1)


class Publisher(db.Model):
    __tablename__ = 'publisher'

    name = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    books = db.relationship('Book', backref = 'publisher') # (1-M)


class BookDetails(db.Model):
    __tablename__ = 'bookdetails'

    id = db.Column(db.Integer, primary_key = True) 
    detail = db.Column(db.String(255))
    book_id = db.Column(db.Integer, db.ForeignKey('book.id')) # (1-1)


class Author(db.Model):
    __tablename__ = 'author'
	
    name = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    # This attribute connects an Author object, a Book object, 
    # and the table link 
    books = db.relationship('Book', secondary = 'link', backref='wrote') # (M-M)


class BookSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Book
        include_relationships = True
        load_instance = True
        include_fk = True

class AuthorSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Author
        include_relationships = True
        load_instance = True
        include_fk = True

class PublisherSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Publisher
        include_relationships = True
        load_instance = True
        include_fk = True


if __name__ == "__main__":
    db.drop_all()
    db.create_all()