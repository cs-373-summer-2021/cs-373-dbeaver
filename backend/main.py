from flask import render_template, jsonify
from models import app, db, Book, Author, Publisher, BookSchema, AuthorSchema, PublisherSchema


author_schema = AuthorSchema()
publisher_schema = PublisherSchema()
book_schema = BookSchema()

# ------------
# index
# ------------
@app.route('/')
def temp():
    return render_template("temp.html")

# ------------
# book
# ------------	
@app.route('/books/')
def book():
	book_list = db.session.query(Book).all()
	return render_template('books.html', book_list = book_list)

# ------------
# bookjson
# ------------	
@app.route('/books/json/')
def bookjson():
    """
    turn your books of objects into a list of serializable values
    """
    result_list = db.session.query(Book).all()
    result = []
    for elem in result_list:
        result.append(book_schema.dump(elem))
    return jsonify(result)

# ------------
# authorjson
# ------------	
@app.route('/authors/json/')
def authorjson():
    """
    turn your authors of objects into a list of serializable values
    """
    result_list = db.session.query(Author).all()
    result = []
    for elem in result_list:
        result.append(author_schema.dump(elem))
    return jsonify(result)


# ------------
# publisherjson
# ------------	
@app.route('/publisher/json/')
def publisherjson():
    """
    turn your publishers of objects into a list of serializable values
    """
    result_list = db.session.query(Publisher).all()
    result = []
    for elem in result_list:
        result.append(publisher_schema.dump(elem))
    return jsonify(result)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
