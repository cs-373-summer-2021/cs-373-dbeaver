#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/create_db.py
# Fares Fraij
# ---------------------------

import json
from models import db, BookDetails, Book, Author, Publisher

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_data
# ------------
def create_data():
    """
    populate book table
    """

    # -- primary objects --

    # create three Author objects
    author1 = Author(name = 'author1', id = 1 )
    author2 = Author(name = 'author2', id = 2 )
    author3 = Author(name = 'author3', id = 3 )

    db.session.add(author1)
    db.session.add(author2)
    db.session.add(author3)
    db.session.commit()

    # create two Book objects
    book1 = Book(title = 'book1', id = 1, price = '$5')
    book2 = Book(title = 'book2', id = 2, price = '$12')
    book3 = Book(title = 'book3' , id = 3, price = '$8')

    db.session.add(book1)
    db.session.add(book2)
    db.session.add(book3)
    db.session.commit()

     # creating bookDetails3, bookDetails3, bookDetails3
    bookDetails1 = BookDetails(id = 1, detail = 'details of book 1')
    bookDetails2 = BookDetails(id = 2, detail = 'details of book 2')
    bookDetails3 = BookDetails(id = 3, detail = 'details of book 3')

    db.session.add(bookDetails1)
    db.session.add(bookDetails2)
    db.session.add(bookDetails3)
    db.session.commit()

    # creating publisher1, publisher 2
    publisher1 = Publisher(name = 'publisher1', id = 1 )
    publisher2 = Publisher(name = 'publisher2', id = 2 )

    db.session.add(publisher1)
    db.session.add(publisher2)
    db.session.commit()

    # -- relationships --

    # (M-M) (Author-Book)
    book1.wrote.append(author1)
    db.session.commit() 
    
    book1.wrote.append(author2)
    db.session.commit()
    
    book1.wrote.append(author3)
    db.session.commit()

    book2.wrote.append(author2)
    db.session.commit()

    book2.wrote.append(author1)
    db.session.commit()

    book3.wrote.append(author3)
    db.session.commit()

    # (1-1) (Book-BookDetails)
    # You cannot use 'newBook1.details.append(bookDetails1)" since 
    # the property 'uselist' is set to False, i.e., One-To-One mapping
    # You can simply assign bookDetails1 object to 
    # the property newBook1.details and SQLAlchmey will 
    # automatically take care of the foreign key relationship.    
    book1.details = bookDetails1
    book2.details = bookDetails2
    book3.details = bookDetails3
    db.session.commit()

    # (1-M) (Publisher-Book)
    publisher1.books.append(book1)
    publisher1.books.append(book2)
    publisher2.books.append(book3)
    db.session.commit()


        
if __name__ == '__main__':
    print("Entered")
    db.drop_all()
    print("Dropped_all")
    db.create_all()
    print("Created_all")
    create_data()
    print("Done")
